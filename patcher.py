import sys, os, hashlib, csv

def sha1sum(filename):
        sha = hashlib.sha1()
        file = open(filename, "rb")
        buff_size = 65536
        while True:
                data = file.read(buff_size)
                if not data:
                        break
                sha.update(data)
        file.close()
        return sha.hexdigest()


class Files(object):
        def __init__(self):
                self.filelist = []

        def scan_dir(self, directory):
        # Fill the structure with data
        # TODO: check for Linux compatibility
        # TODO: ignore files/folders
                for root, dirs, files in os.walk(directory):
                        for f in files:
                                path = os.path.join(root, f)
                                size = os.path.getsize(path)
                                try:
                                        checksum = sha1sum(path)
                                except IOError:
                                        checksum = "IOError"

                                path = os.path.relpath(path, directory)
                                print path, checksum
                                self.filelist.append([1, path, checksum])


        def out(self): # rename
                for f in self.filelist:
                        yield f


        def insert(self): # rename
        # add files to structure (say...from a csv file)
                pass

        def search(self):
        # a function to search the structure
                pass


def compare():
        pass

def csv_write(obj):
        # Writes a csv file where the schema is:
        # filetype, file location relative the root dir, filename in archive
        # filetype = {0: "deleted file", 1: "copy/overwrite", 2: "delta"}
        out = csv.writer(open("pgen.csv", 'wb'), delimiter=',')
        for item in obj.out():
                out.writerow([item[0], item[1], item[2]])

def csv_read():
        pass

def usage():
        print("usage: not yet written")

def main(argv):
        # rewrite to use proper options
        # options are needed
        # in particular:
        # Delta files
        # ignore files/folders

        if len(argv) == 0:
                # Start gui
                # otherwise assume cli mode when there are arguments
                # Load Tkinker here rather than globally
                print("GUI not yet written.\nPoke and prod the programmer"),
                print("to get it implemented faster.")
                raw_input("press any key to close...")
                sys.exit(0)

        elif len(argv) == 1:
                # if ignore file is specified
                # load it and pass it to scan_dir()
                source = Files()
                source.scan_dir(argv[0])

                # export a file list
                csv_write(source)

        elif len(argv) == 2:
                scan_dir()
                csv_read()
                compare()

        else:
                usage()
                sys.exit(1)

if __name__ == "__main__":
        main(sys.argv[1:])
